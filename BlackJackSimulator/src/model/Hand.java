package model;

import java.util.Collections;
import java.util.Vector;

public class Hand
{
	private int myMaxNumberCards;
	protected Vector<Card> myCards;

	public Hand(int startingCards)
	{
		myMaxNumberCards = startingCards;
		myCards = new Vector<Card>(myMaxNumberCards,1);
	}
	
	public boolean add(Card card)
	{
		myCards.add(card);
		return true;
	}
	
	public String toString()
	{
		String str = "Hand containing";
		for(Card c : myCards)
		{
			str = str + " " + c.toString();
		}
		return str;
	}

	public void orderCards()
	{
		Collections.sort(myCards);
	}

	public int getNumberCardsInHand()
	{
		return myCards.size();
	}

	public Vector<Card> getCards()
	{
		return myCards;
	}
	
	public int sumHand()
	{
		int sum = 0;
		
		for(int i = 0; i < getCards().size(); i++)
		{
			sum = sum + getCards().get(i).getType().getType();
		}
		return sum;
	}
	
}
//