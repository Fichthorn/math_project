package model;

import java.util.Collections;

import java.util.Vector;

public class Deck
{
	private Vector<Card> myCards;
	private final int myFullDeckSize = 52;

	public Deck()
	{
		myCards=new Vector<Card>();
		constructDeck();
		shuffle();
	}
	
	public boolean constructDeck()
	{
		for(CardSuit suit: CardSuit.values())
		{
			for(CardType type: CardType.values())
			{
				myCards.add(new Card(suit,type));
			}
		}
		return true;
	}
	
	public Card draw()
	{
		if(myCards.isEmpty())
		{
			return null;
		}
		else
		{
			Card card = myCards.remove(0);
			return card;
		}
	}
	
	public boolean shuffle()
	{
		Collections.shuffle(myCards);
		return true;
	}
	
	public int getFullDeckSize()
	{
		return myFullDeckSize;
	}
	
	public Vector<Card> getCards()
	{
		return myCards;
	}
	
	public String toString()
	{
		String str = "Deck has "+getCards().size()+"cards containing: ";
		for(Card c : this.myCards)
		{
			str = str +" "+ c.toString();
		}
		return str;
	}
	
	public Object clone()
	{
		Deck myCloneDeck = new Deck();
		for(int i = 0;i<myFullDeckSize;i++)
		{
			myCloneDeck.getCards().insertElementAt(myCards.elementAt(i), i);
		}
		return myCloneDeck;
	}
	
	public boolean isEmpty()
	{
		if(myCards.isEmpty())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
//