package model;

import java.util.Vector;

public class BlackJackSimulator
{
	private static Player[] myPlayers;
	private static Deck myDeck0;
	private static Deck myDeck1;
	private static int decksPlayed;
	private static int handsPlayed;
	private static int sum;
	private int[] playerHands;
	private int[] cPlayerHands;
	private static Vector<Integer> pVector;
	private static Vector<Integer> cpVector;
	private static int handsPerDeck, pWinsPerDeck, cpWinsPerDeck, ties, winsPerDeck, lossesPerDeck, tiesPerDeck, wins, losses;

	public BlackJackSimulator()
	{
		myPlayers = new Player[2];
		myPlayers[0] = new Player(false);
		myPlayers[1] = new Player(true);

		myDeck0 = new Deck();
		myDeck1 = new Deck();
		myDeck0.shuffle();
		myDeck1.shuffle();
		
		pVector = new Vector<Integer>(250);
		cpVector = new Vector<Integer>(250);

		decksPlayed = 0;
		handsPlayed = 0;
		
		handsPerDeck = 0;
		pWinsPerDeck = 0;
		cpWinsPerDeck = 0;
		ties = 0;
		winsPerDeck = 0;
		lossesPerDeck = 0;
		tiesPerDeck = 0;
		wins = 0;
		losses = 0;
	}

	public static void main(String args[])
	{	
		BlackJackSimulator BJS = new BlackJackSimulator();
		BJS.playTwoDecks();
	}

	public static boolean hitMe(int i)
	{
		boolean bust = false;
		sum = getPlayer(i).getHand().sumHand(); 

		System.out.print(getPlayer(i).getName() + " has" + 
				getCard(i, 0).toString() + " and"
				+ getCard(i, 1).toString() + ". Sum: "
				+ sum);

		while(getPlayer(i).getHand().sumHand() < getPlayer(i).getHitMe())
		{
			getPlayer(i).getHand().add(chooseDeck().draw());

			sum = getPlayer(i).getHand().sumHand();
			System.out.print(". Hit! New card:" + getCard(i, getPlayer(i).getHand().getCards().size() - 1).toString()
					+ " sum:" + sum + " ");
		}

		if(sum > 21)
		{
			bust = true;
			System.out.print(" Bust!");
		}
		
		if(!getPlayer(i).getAmAI())
		{
			pVector.add(sum);
		}
		else
		{
			cpVector.add(sum);
		}
		
		if(i == 1 && getPlayer(0).getHand().sumHand() < 21 && 21 > getPlayer(1).getHand().sumHand())
		{
			if(getPlayer(0).getHand().sumHand() > getPlayer(1).getHand().sumHand())
			{
				getPlayer(0).incrementNumberWins();
				winsPerDeck++;
				wins++;
				System.out.println();
				System.out.print("win!");
			}
			else if(getPlayer(0).getHand().sumHand() < getPlayer(1).getHand().sumHand())
			{
				getPlayer(1).incrementNumberWins();
				lossesPerDeck++;
				losses++;
				System.out.println();
				System.out.print("loss!");
			}
			else
			{
				ties++;
				tiesPerDeck++;
				System.out.println();
				System.out.print("tie!");
			}
		}
		else if(getPlayer(0).getHand().sumHand() > 21 && 21 < getPlayer(1).getHand().sumHand())
		{
			ties++;
			tiesPerDeck++;
		}

		handsPerDeck ++;
		System.out.println();

		return bust;
	}

	public static Player getPlayer(int i)
	{
		return myPlayers[i];
	}

	public static Deck chooseDeck()
	{
		if(!myDeck0.isEmpty())
		{
			return getDeck0();
		}
		else if(!myDeck1.isEmpty())
		{
			return getDeck1();
		}
		else
		{
			decksPlayed += 2;
			System.out.println();
			System.out.println();
			System.out.println("finished decks " + (decksPlayed - 1) + " and " + (decksPlayed) + ". stats for deck is: ");
			
			System.out.println("hands played for decks: " + (handsPerDeck / 2));
			System.out.println("hands won and lost for deck: " + winsPerDeck + " wins and " + lossesPerDeck + " losses and " + tiesPerDeck + " ties");
			System.out.println("money for deck: " + (2 * (winsPerDeck - lossesPerDeck)) + " dollars");
			
			System.out.println("hands played to date: " + (handsPlayed / 2) + "hands played");
			System.out.println("hands won and lost to date: " + wins + " wins and " + losses + " losses and " + ties + " ties to date");
			System.out.println("money to date: " + (2 * (wins - losses)) + " dollars");
			
			System.out.println();
			myDeck0.constructDeck();
			myDeck1.constructDeck();
			myDeck0.shuffle();
			myDeck1.shuffle();
			handsPerDeck = 0;
			pWinsPerDeck = 0;
			cpWinsPerDeck = 0;
			winsPerDeck = 0;
			lossesPerDeck = 0;
			tiesPerDeck = 0;
			return chooseDeck();
		}
	}

	public static Deck getDeck0()
	{
		return myDeck0;
	}

	public static Deck getDeck1()
	{
		return myDeck1;
	}
	public static Card getCard(int i, int j)
	{
		return getPlayer(i).getHand().getCards().get(j);
	}

	public static void clearHand(int i)
	{
		getPlayer(i).getHand().getCards().clear();
	}

	public void playTwoDecks()
	{
		while(decksPlayed != 24)
		{
			for(int i = 0; i < 2; i++)
			{
				getPlayer(i).getHand().add(chooseDeck().draw());
				getPlayer(i).getHand().add(chooseDeck().draw());
				hitMe(i);
				if(i == 1)
				{
					clearHand(0);
					clearHand(1);
				}
				
				handsPlayed++;
			}
		}
		pVector.sort(null);
		cpVector.sort(null);
		playerHands = new int[pVector.size()];
		cPlayerHands = new int[cpVector.size()];
		
		for(int i = 0; i < pVector.size(); i++)
		{
			playerHands[i] = pVector.get(i);
			cPlayerHands[i] = cpVector.get(i);
		}
		
		System.out.println();
		System.out.println(money(0));
		System.out.println(money(1));
		System.out.println("card stats");
		System.out.println("player min value: " + playerHands[0]);
		System.out.println("player max value: " + playerHands[pVector.size() - 1]);
		System.out.println("player mean value: " + meanHandValue(playerHands));
		System.out.println("player median value: " + medianHandValue(playerHands));
		System.out.println("player mode value: " + modeHandValue(playerHands));
		System.out.println("player standard deviation value: " + standardDeviationHandValue(playerHands));
		
		System.out.println("dealer min value: " + cPlayerHands[0]);
		System.out.println("dealer max value: " + cPlayerHands[cpVector.size() - 1]);
		System.out.println("dealer mean value: " + meanHandValue(cPlayerHands));
		System.out.println("dealer median value: " + medianHandValue(cPlayerHands));
		System.out.println("dealer mode value: " + modeHandValue(cPlayerHands));
		System.out.println("dealer standard deviation value: " + standardDeviationHandValue(cPlayerHands));
	}

	/**
	 * calculate the mean of an array of integers
	 * 
	 * @param m
	 * @return mean
	 */
	public static double meanHandValue(int[] m)
	{
		int sum = 0;

		for (int i = 0; i < m.length; i++)
		{
			sum += m[i];
		}

		return (sum / m.length);
	}

	/**
	 * same thing, but with a double array instead of int!
	 * 
	 * @param m
	 * @return mean
	 */
	public static double mean(double[] m)
	{
		int sum = 0;

		for (int i = 0; i < m.length; i++)
		{
			sum += m[i];
		}

		return (sum / m.length);
	}

	/**
	 * returns the median from an array of integers
	 * if the middle is between two numbers, returns
	 * the avg. between the two. must be sorted
	 * 
	 * @param m
	 * @return median
	 */
	public static double medianHandValue(int[] m)
	{
		int middle = m.length/2;

		if(m.length%2 == 1)
		{
			return m[middle];
		}
		else
		{
			return (m[middle-1] + m[middle]) / 2.0;
		}
	}

	/**
	 * returns the mode from an array of integers
	 * 
	 * @param a
	 * @return mode
	 */
	public static double modeHandValue(int a[])
	{
		int maxValue = 0;
		int maxCount = 0;

		for(int i = 0; i < a.length; ++i)
		{
			int count = 0;

			for(int j = 0; j < a.length; ++j)
			{
				if (a[j] == a[i]) count++;
			}

			if(count > maxCount)
			{
				maxCount = count;
				maxValue = a[i];
			}
		}

		return maxValue;
	}

	/**
	 * uses four steps to calculate standard deviation.
	 * 1. find mean
	 * 2. subtract mean from each value and square the result
	 * 3. find mean of results
	 * 4. sqrt 3, and done!
	 * 
	 * @param a
	 * @return standard deviation
	 */
	public static double standardDeviationHandValue(int a[])
	{
		//find mean
		double avg = meanHandValue(a);
		double stDv = 0;
		double stDvArray[] = new double[100];

		//subtract mean from each value and square the result
		for(int i = 0; i < 100; i++)
		{
			stDvArray[i] = (a[i] - avg);
			stDvArray[i] = stDvArray[i] * stDvArray[i];
		}

		//find the mean of results!
		stDv = mean(stDvArray);

		//sqrt that, then done!
		stDv = Math.sqrt(stDv);

		return stDv;
	}
	
	public int money(int i)
	{
		int maxPotential = handsPlayed * 2;
		int moneyLost = maxPotential - getPlayer(i).getNumberWins() * 2;
		int moneyEarned = getPlayer(i).getNumberWins() * 2;
		return moneyEarned- moneyLost;
	}
}
//