package model;

public class Card implements Comparable<Card>
{
  private CardSuit mySuit;
  private CardType myType;
  private boolean myIsSelected;
  private boolean myIsFaceUp;

  public Card(CardSuit suit, CardType type) 
  {
	  mySuit=suit;
	  myType=type;
	  myIsSelected=false;
	  myIsFaceUp=false;
  }

  public void flip() 
  {
	  myIsFaceUp = !myIsFaceUp;
  }

  public boolean isFaceUp() 
  {
	  return myIsFaceUp;
  }

  public boolean isSelected() 
  {
	  return myIsSelected;
  }
  
  public void toggleSelected() 
  {
	  myIsSelected = !myIsSelected;
  }
  
  public int compareTo(Card card) 
  {
	  if(myType.getType() > card.getType().getType())
	  {
		  return 1;
	  }
	  if(myType.getType() == card.getType().getType())
	  {
		  return 0;
	  }
	  if(myType.getType() < card.getType().getType())
	  {
		  return -1;
	  }
	  return 0;
  }

  public Object clone() 
  {
	  Card myClone;
	  CardType myCloneType = myType;
	  CardSuit myCloneSuit = mySuit;
	  myClone = new Card(myCloneSuit,myCloneType);
	  return myClone;
  }

  public String toString() 
  {
	  return " a "+getType()+" of "+getSuit();
  }
  
  public CardSuit getSuit()
  {
	  return mySuit;
  }
  
  public CardType getType()
  {
	  return myType;
  }
  
}
//