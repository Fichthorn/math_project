package model;

public class Player
{
	private String myName;
	private int myNumberWins, hitMe;
	protected boolean myAmAI;
	protected Hand myHand;

	public Player(boolean amAI)
	{
		
		myNumberWins = 0;
		myAmAI = amAI;
		
		if(!myAmAI)
		{
			hitMe = 16;
			myName = "player";
		}
		else
		{
			hitMe = 17;
			myName = "computer";
		}
		
		myHand = new Hand(2);
	}

	public int incrementNumberWins()
	{
		myNumberWins++;
		return myNumberWins;
	}

	public String toString()
	{
		return myName;
	}

	public Hand getHand()
	{
		return myHand;
	}

	public String getName()
	{
		return myName;
	}

	public int getNumberWins()
	{
		return myNumberWins;
	}
	
	public void setNumberWins()
	{
		myNumberWins = 0;
	}

	public boolean getAmAI()
	{
		return myAmAI;
	}
	
	public int getHitMe()
	{
		return hitMe;
	}

}
//